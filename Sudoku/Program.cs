﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sudoku
{
    class Program
    {
		public static int bestewaardesudoku;

		static void Main(string[] args)
        {
            // vb om te testen 
            //int dimensie = 4;
            //int[,] inhoud = new int[4, 4] { { 1, 2, 3, 4 }, { 2, 3, 4, 1 }, { 3, 4, 1, 2 }, { 4, 1, 2, 3 } };

            //bool[,] gefixeerdd = new bool[dimensie, dimensie];  // bevat posities met true, waar getallen gefixeerd zijn
            //gefixeerdd = new bool[4, 4] { { true, false, false, false }, { false, false, false, false }, { false, false, false, false }, { false, false, false, false } };
            //int[,] testen = swapp(inhoud, gefixeerdd);
            //int testen2 = evalueer(testen);


            int dimensie = 9;
			

            int[,] inhoud = new int[dimensie, dimensie];
            bool[,] gefixeerd = new bool[dimensie, dimensie];  // bevat posities met true, waar getallen gefixeerd zijn

            if (dimensie == 9)                             // bij dimensie 9 niet op spaties splitsen -- bij dimensie 16 wel
            {                                             // nog erbij: voor meerdere grids: vragen aan ta of we vooraf weten
                                                          // hoeveel sudoku's per tekstbestand, verschilt namelijk bij 9 en 16 ..
                for (int i = 0; i < dimensie; i++)
                {
                    int[] hrij;                            //  horizontale rij
                    string input = Console.ReadLine();
                    char[] hor = input.ToCharArray();
                    hrij = Array.ConvertAll(hor, c => (int)Char.GetNumericValue(c));

                    for (int j = 0; j < dimensie; j++)
                    {
                        inhoud[i, j] = hrij[j];

                        if (hrij[j] != 0)                // als getal niet 0 is dan moeten de positie in de gefixeerd array op true gezet worden
                        {
                            gefixeerd[i, j] = true;
                        }

                    }
                }
            }

            int[,] sudokiInitil = initialisatie(inhoud, gefixeerd);


			// Swapp herhaaldelijk uitvoeren: random blok kiezen, en beste swap binnen dat blok uitvoeren
			// Testen of de evaluatiefunctie onveranderd blijft, vast in (lokale) minima


			// Random Walk:  Of 1 random swap en k keer random blok kiezen?

			
			int counter = 0;
			int vorigewaarde = evalueer(sudokiInitil);
			bool doorgaan = true;

			while (doorgaan)
            {
				int[,] geswaptesudoku = swapp(sudokiInitil, gefixeerd);
				
				if (vorigewaarde == bestewaardesudoku)
				{
					counter++;
				}
				else
				{
					counter = 0;
				}
				vorigewaarde = bestewaardesudoku; 

				if(bestewaardesudoku == 0) { break; }

				if(counter > 50)
				{
					//randomwalk;
					
				}

				if (counter > 100)   // hier hebben we de hoop opgegeven
				{
					
					doorgaan = false;
				}
			}

		

			int[,] randomWalk(int[,] sudokuOmteSwappen, bool[,] ishetGefixeerd)
			{
				int[,] resultaat;
				int[] mogelijkheden;
				
				if (dimensie == 9)
				{
					mogelijkheden = new int[] { 0, 3, 6 };
				}

				else { mogelijkheden = new int[] { 0, 4, 8, 12 }; }
				int[] mog = new int[] { 0, 1, 2 };

				//maak de random jongen aan en selecteer een random blok
				Random r = new Random();
				int i = mogelijkheden[r.Next(mogelijkheden.Length)];
				int j = mogelijkheden[r.Next(mogelijkheden.Length)];

				
				//selecteer een random 1e coordinaat in het random blok;
				int x1 = i + mog[r.Next(mog.Length)];
				int y1 = j + mog[r.Next(mog.Length)];
				while (ishetGefixeerd[x1, y1] == true)
				{
					x1 = i + mog[r.Next(mog.Length)];
					y1 = j + mog[r.Next(mog.Length)];
				}

				//selecteer een random 2e coordinaat in het random blok;
				int x2 = i + mog[r.Next(mog.Length)];
				int y2 = j + mog[r.Next(mog.Length)];
				while (ishetGefixeerd[x2, y2] == true)
				{
					x2 = i + mog[r.Next(mog.Length)];
					y2 = j + mog[r.Next(mog.Length)];
				}

				//swap de waardes van de twee coordinaten
				int eersteWaarde = sudokuOmteSwappen[x1, y1];
				int tweedeWaarde = sudokuOmteSwappen[x2, y2];

				sudokuOmteSwappen[x1, y1] = tweedeWaarde;
				sudokuOmteSwappen[x2, y2] = eersteWaarde;


				resultaat = sudokuOmteSwappen;
				return resultaat;
			}






			// initialisatie methode vult de 0'en in de sudoku met een legale getal binnen een blok van 3x3 bij sudoku van 9

			int[,] initialisatie(int[,] sudokeOmInTevullen, bool[,] ishetGefixeerd)
            {
                int[,] resultaat;

                // vul de blokken in, in geval van dimensie=9, zijn dit 9 blokken van 3x3

                int[] mogelijkheden = new int[] { 0, 3, 6 };   // dit algemener maken


                for (int la = 0; la < mogelijkheden.Length; la++)
                {
                    for (int wa = 0; wa < mogelijkheden.Length; wa++)
                    {
                        int[] availableNumbers = (Enumerable.Range(1, dimensie)).ToArray();    // de getallen om sudoku mee in te vullen

                        for (int g = mogelijkheden[la]; g < mogelijkheden[la] + (Math.Sqrt(dimensie)); g++)
                        {
                            for (int f = mogelijkheden[wa]; f < mogelijkheden[wa] + (Math.Sqrt(dimensie)); f++)
                            {
                                if (ishetGefixeerd[g, f] == true)   // als t gefixeerd getal is kunnen we die niet meer gebruiken om vak verder in te vullen
                                {
                                    availableNumbers[sudokeOmInTevullen[g, f] - 1] = 0;   // in de availablenumbers worden getallen die we niet mogen gebruiken 0
                                }                                                       // availablenumbers[0] = 1, dus hierboven -1 
                            }
                        }

                        for (int g = mogelijkheden[la]; g < mogelijkheden[la] + (Math.Sqrt(dimensie)); g++)
                        {
                            for (int f = mogelijkheden[wa]; f < mogelijkheden[wa] + (Math.Sqrt(dimensie)); f++)
                            {
                                if (ishetGefixeerd[g, f] == false)
                                {
                                    int tellertje = 0;

                                    while (availableNumbers[tellertje] == 0)
                                    {
                                        tellertje++;
                                    }
                                    sudokeOmInTevullen[g, f] = availableNumbers[tellertje];
                                    availableNumbers[tellertje] = 0;
                                }

                            }
                        }
                    }
                }

                resultaat = sudokeOmInTevullen;

                return resultaat;
            }


            // evalueer sudoku

            int evalueer(int[,] sudokuEvalueren)
            {
                int score = 0;     // we beginnen met 0 conflicten

                for (int i = 0; i < dimensie; i++)         // rijen langs
                {
                    var rijenlist = new List<int>();
                    var kolommenlist = new List<int>();

                    for (int s = 0; s < dimensie; s++)
                    {
                        rijenlist.Add(sudokuEvalueren[i, s]);

                        kolommenlist.Add(sudokuEvalueren[s, i]);
                    }

                    var getallendieontbrekenrij = Enumerable.Range(1, dimensie).Except(rijenlist);
                    var getallendieontbrekenkol = Enumerable.Range(1, dimensie).Except(kolommenlist);
                    score += getallendieontbrekenrij.Count();
                    score += getallendieontbrekenkol.Count();
                }
                return score;
            }


            //swap methode, kiest een random blok van wortel n x wortel n = 3x3 bij dimensie 9, en probeert alle mogelijke swaps
            //kiest de swap die voor minste conflicten zorgt, past die toe, geeft de sudoku terug na die swap

            int[,] swapp(int[,] sudokuOmteSwappen, bool[,] ishetGefixeerd)
            {
                int[,] resultaat;
                int[] mogelijkheden;

                // swappen, daarna sudoku evalueren en als current best opslaan, als er een betere evaluatie is vervangen

                if (dimensie  == 9)
                {
                    mogelijkheden = new int[] { 0, 3, 6 };    // om een blok te bepalen, met linkerboven coordinaat 0,0 bijv of 6,0 (blok rechtsonder)
                }                                             
                else { mogelijkheden = new int[] { 0, 4, 8, 12}; }
                

                Random r = new Random();
                int i = mogelijkheden[r.Next(mogelijkheden.Length)];
                int j = mogelijkheden[r.Next(mogelijkheden.Length)];

                bool[,] gehadofniet = new bool[dimensie, dimensie];   // uit de for-loop gehaald, nu werkt het, want er werd steeds een nieuwe
                                                                      // gehadofniet aangemaakt als je de volgende niet-gefixeerde getal pakte
                                                                      // dus had het geen nut
                int huidigewaardeSudoku = evalueer(sudokuOmteSwappen);
                int coordinaat1 = 0;      int coordinaat2 = 0;
                int coordinaat3 = 0;      int coordinaat4 = 0;

                for (int z = i; z < (i + Math.Sqrt(dimensie)); z++)
                {
                    for (int q = j; q < (j + Math.Sqrt(dimensie)); q++)
                    {          
                        if (ishetGefixeerd[z, q] == false)     // als de positie in het blok niet gefixeerd is
                        {
                            gehadofniet[z, q] = true;

                            for (int h = i; h < (i + Math.Sqrt(dimensie)); h++)    // doe alle swaps met de rest van de niet gefixeerde posities
                            {
                                for (int ag = j; ag < (j + Math.Sqrt(dimensie)); ag++)
                                {
                                    if (ishetGefixeerd[h, ag] == false && gehadofniet[h, ag] == false)
                                    {
                                        int temp = sudokuOmteSwappen[z, q];
                                        int temp2 = sudokuOmteSwappen[h, ag];

                                        sudokuOmteSwappen[z, q] = temp2;
                                        sudokuOmteSwappen[h, ag] = temp;

                                        // evalueren                                         
                                        int waardeNieuweSudoku = evalueer(sudokuOmteSwappen); 
                                        if (waardeNieuweSudoku <= huidigewaardeSudoku)
                                        {
                                            huidigewaardeSudoku = waardeNieuweSudoku;
                                            coordinaat1 = z;    coordinaat2 = q;  // update de coordinaten, zodat we weten welke swap we moeten uitvoeren later
                                            coordinaat3 = h;    coordinaat4 = ag;

											bestewaardesudoku = waardeNieuweSudoku;

										}
                                         
                                        // terug swappen:
                                        sudokuOmteSwappen[z, q] = temp;
                                        sudokuOmteSwappen[h, ag] = temp2;

                                    }
                                }
                            }
                        }
                    }
                }

                // de beste swap daadwerkelijk toepassen en teruggeven
                int temppp = sudokuOmteSwappen[coordinaat1, coordinaat2];
                int temppp2 = sudokuOmteSwappen[coordinaat3, coordinaat4];
                sudokuOmteSwappen[coordinaat1, coordinaat2] = temppp2;
                sudokuOmteSwappen[coordinaat3, coordinaat4] = temppp; 

                resultaat = sudokuOmteSwappen;
                return resultaat;
            }




            // om sudoku uit te printen

            //for (int i = 0; i < dimensie; i++)
            //{
            //    for (int j = 0; j < dimensie; j++)
            //    {
            //        Console.Write(string.Format("{0} ", testen[i, j]));
            //    }
            //    Console.Write(Environment.NewLine + Environment.NewLine);
            //}

            //Console.WriteLine("\n");
            //Console.WriteLine(testen2);



        }
    }
}
